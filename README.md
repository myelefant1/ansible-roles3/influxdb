## Influxdb ansible role

This roles:
  * installs influxdb server
  * could installs influxdb client
  * could create database on influxdb server

Tested on debian 10

See file [tests/tests_influxdb](tests/tests_influxdb) for more examples.

## Role parameters

| name                    | typee       | mandatory value     | default value | description                                       |
| ------------------------|-------------|---------------------|-------------------------------------------------------------------|
| influxdb_database       | string      | no                  | N/A           | if specified, `influxdb_database` will be created |

## Using this role

### ansible galaxy
Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/ansible-roles3/influxdb.git
  scm: git
  version: v1.2
```

### Playbook
```
- hosts: all
  roles:
    - role: influxdb
      influxdb_install_client: false
```
